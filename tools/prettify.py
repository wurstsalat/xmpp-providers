#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>
# SPDX-FileCopyrightText: 2021 Michel Le Bihan <michel@lebihan.pl>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
Validates JSON files and applies a consistent format to them.

It is intended as a Git pre-commit hook.
"""

from typing import cast

import logging
import sys

from tools.common import CLIENTS_FILE_PATH
from tools.common import convert_dict_to_json_string
from tools.common import load_json_file
from tools.common import PROPERTIES_FILE_PATH
from tools.common import PROVIDERS_FILE_PATH

log = logging.getLogger()


class Prettify:
    """Validates JSON files and applies a consistent format to them.

    Parameters
    ----------
    file_paths : list[str]
        paths of files to be prettified (default: all relevant files)
    quit_with_exit_code : bool
        whether it should return with one of the following exit codes after
        termination (default: True):
            0: The content was correctly formatted before.
            1: The content was not correctly formatted before and has been formatted or
               could not be formatted.
    """

    def __init__(
        self,
        file_paths: list[str] | None = None,
        quit_with_exit_code: bool = True,
    ) -> None:
        if file_paths is None:
            file_paths = [PROVIDERS_FILE_PATH, CLIENTS_FILE_PATH]

        log.info("Starting prettifying: %s", file_paths)

        json_data = load_json_file(PROPERTIES_FILE_PATH)

        if json_data is None:
            if quit_with_exit_code:
                sys.exit(1)
            return

        self._properties = cast(list[str], json_data)
        all_files_already_correct = True

        for json_file_path in file_paths:
            if not self._prettify(json_file_path):
                all_files_already_correct = False

        if not all_files_already_correct and quit_with_exit_code:
            sys.exit(1)

    def _prettify(self, json_file_path: str) -> bool:
        """Validates the JSON file from json_file_path and applies a consistent
        format to it.

        Parameters
        ----------
        json_file_path : str
            path of the JSON file to be validated and formatted

        Returns
        -------
        bool
            whether the JSON file was already correctly formatted before
        """

        json_data = load_json_file(json_file_path)

        if json_data is None:
            return False

        unsorted_entries = json_data.items()

        # Entries (providers or clients) are sorted in alphabetically ascending
        # and lowercase first order by their keys (domains or names).
        # Properties are sorted in the order specified by the file with
        # PROPERTIES_FILE_PATH.
        sorted_entries = {
            entry_key: {
                property_key: properties[property_key]
                for property_key in self._properties
                if property_key in properties
            }
            for entry_key, properties in sorted(
                unsorted_entries, key=lambda item: str.swapcase(item[0])
            )
        }

        properties_missing = False

        # Check if provider entries have all needed properties.
        if json_file_path == PROVIDERS_FILE_PATH:
            for provider, properties in sorted_entries.items():
                for property_key in self._properties:
                    if property_key not in properties.keys():
                        log.error(
                            "%s: Property '%s' is missing",
                            provider,
                            property_key,
                        )
                        if not properties_missing:
                            properties_missing = True

        formatted_json_string = convert_dict_to_json_string(sorted_entries)

        try:
            with open(json_file_path, "r+") as json_file:
                original_json_string = json_file.read()

                if (
                    original_json_string == formatted_json_string
                    and not properties_missing
                ):
                    log.debug("'%s' is already correctly formatted", json_file_path)
                else:
                    json_file.seek(0)
                    json_file.write(formatted_json_string)
                    json_file.truncate()

                    if properties_missing:
                        log.error(
                            "'%s' has missing provider properties: "
                            "Add them before the next commit",
                            json_file_path,
                        )

                    if original_json_string != formatted_json_string:
                        log.info(
                            "'%s' has been formatted: Stage changed lines by "
                            "'git add -p %s' and run 'git commit'",
                            json_file_path,
                            json_file_path,
                        )

                    return False

        except OSError as e:
            log.error(f"File '{json_file_path}' could not be loaded: {e}")
            return False

        return True
