#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Daniel Brötzmann <daniel.broetzmann@posteo.de>
# SPDX-FileCopyrightText: 2023 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Contains methods for working with the provders file."""

from __future__ import annotations

from typing import Any
from typing import cast

import logging
import os
from enum import Enum
from subprocess import call

from tools.common import convert_dict_to_json_string
from tools.common import CRITERIA_FILE_PATH
from tools.common import load_json_file
from tools.common import PROPERTIES_FILE_PATH
from tools.common import PROVIDERS_FILE_PATH
from tools.common import README_FILE_PATH
from tools.data_types import ProviderDetailsT
from tools.prettify import Prettify

ACTION_ADD_PROPERTY = "a"
ACTION_REMOVE_PROPERTY = "r"
ACTION_QUIT = "q"
STRING_TRUE = "true"
STRING_FALSE = "false"

log = logging.getLogger()


class InputType(Enum):
    """Data types for user input."""

    STRING = 1
    INTEGER = 2
    BOOLEAN = 3
    LIST = 4
    DICTIONARY = 5
    NULL = 6


class PropertyManager:
    """Manages the properties that providers have."""

    def __init__(self) -> None:
        action = ""
        property_name = ""
        input_type = None

        while True:
            action = self._get_action()

            if action == ACTION_ADD_PROPERTY:
                self._show_title_bar()
                print("Action: Add property\n")

                property_positioned_as_first = self._get_property_positioning()
                property_predecessor = ""

                if not property_positioned_as_first:
                    property_predecessor = self._get_property_predecessor()

                while not property_name:
                    self._show_title_bar()
                    print("Action: Add property\n")
                    property_name = self._get_property_name()

                while True:
                    input_type = self._get_input_type()

                    if input_type is not None and input_type in InputType:
                        break

                property_default = None
                if input_type in (InputType.INTEGER, InputType.BOOLEAN):
                    while property_default is None:
                        property_default = self._get_property_default(input_type)

                property_add_source = None
                while property_add_source is None:
                    property_add_source = self._get_property_add_source()

                self._add_property(
                    property_predecessor,
                    property_name,
                    input_type,
                    property_add_source,
                    property_default,
                )
                self._show_continue_prompt()

                self._show_title_bar()
                action = ""
                property_name = ""
                input_type = ""
                property_default = None
                property_add_source = None

            elif action == ACTION_REMOVE_PROPERTY:
                self._show_title_bar()
                print("Action: Remove property\n")

                while not property_name:
                    self._show_title_bar()
                    print("Action: Remove property\n")
                    property_name = self._get_property_name()

                self._remove_property(property_name)
                self._show_continue_prompt()

                self._show_title_bar()
                action = ""
                property_name = ""

            elif action.lower() == ACTION_QUIT:
                self._clear()
                break

            else:
                self._show_title_bar()
                print("\nUnknown action.\n")
                self._show_continue_prompt()

    def _add_property(
        self,
        property_predecessor: str,
        property_name: str,
        input_type: InputType,
        property_add_source: bool,
        property_default: None | int | bool = None,
    ) -> None:
        """Adds a new property with a name, type and default value.

        Parameters
        ----------
        property_predecessor : str
            name of the property being the new property's predecessor or an empty
            string to position the new property at the first index
        property_name : str
            name of new property
        input_type : InputType
            InputType of new property
        property_add_source : bool
            whether to add a 'source' key for the new property
        property_default : None | int | bool
            default value for new property (only used for integers and booleans)
        """

        self._show_title_bar()
        print("Action: Add property\n")

        if not self._add_property_to_properties_file(
            property_predecessor, property_name
        ):
            print(f"Could not add {property_name} to properties file")
            return

        json_data = load_json_file(PROVIDERS_FILE_PATH)

        if json_data is None:
            return

        providers_data = cast(dict[str, ProviderDetailsT], json_data)

        if property_default is None:
            value = self._convert_input_type_to_python_type(input_type)
        else:
            value = property_default

        for data in providers_data.values():
            if property_add_source:
                data[property_name] = {
                    "content": value,
                    "source": "",
                    "comment": "",
                }
            else:
                data[property_name] = {
                    "content": value,
                    "comment": "",
                }

        try:
            with open(PROVIDERS_FILE_PATH, "w", encoding="utf-8") as providers_file:
                providers_file.write(convert_dict_to_json_string(providers_data))
        except OSError as e:
            print(
                f"The property '{property_name}' could not be added to "
                f"'{PROVIDERS_FILE_PATH}': {e}"
            )

        try:
            log_level = log.getEffectiveLevel()
            log.setLevel(logging.ERROR)
            Prettify(["providers.json"], False)
            log.setLevel(log_level)
        except OSError as e:
            print(
                f"The added property could not be moved to the correct position "
                f"in '{PROVIDERS_FILE_PATH}': {e}"
            )
        except SystemExit as e:
            print(
                f"The added property could not be moved to the correct position "
                f"in '{PROVIDERS_FILE_PATH}': {e}"
            )

        print(f"Property '{property_name}' has been added.")
        print(
            f"Make sure to document the property in '{README_FILE_PATH}' and add "
            f"corresponding criteria to '{CRITERIA_FILE_PATH}'"
        )

    def _convert_input_type_to_python_type(self, input_type: InputType) -> Any:
        """Converst a data type from user input to a Python data type.

        Parameters
        ----------
        input_type : InputType
            data type for which to return a Python data type

        Returns
        -------
        Any
            the initialized Python data type corresponding to input_type
        """

        if input_type == InputType.STRING:
            return ""
        if input_type == InputType.LIST:
            return []
        if input_type == InputType.DICTIONARY:
            return {}

        return None

    def _remove_property(self, property_name: str) -> None:
        """Removes a property by its name.

        Parameters
        ----------
        property_name : str
            name of the property which should be removed
        """

        self._show_title_bar()
        print("Action: Remove property\n")

        if not self._remove_property_from_properties_file(property_name):
            print(f"Could not remove {property_name} from properties file")
            return

        json_data = load_json_file(PROVIDERS_FILE_PATH)

        if json_data is None:
            return

        providers_data = cast(dict[str, ProviderDetailsT], json_data)

        for data in providers_data.values():
            success = data.pop(property_name, None)
            if not success:
                print(f"Could not remove {property_name} from providers file")
                return

        with open(PROVIDERS_FILE_PATH, "w", encoding="utf-8") as providers_file:
            formatted_json_string = convert_dict_to_json_string(providers_data)
            providers_file.write(formatted_json_string)

        print(f"Property '{property_name}' has been removed.")
        print(
            f"Make sure to remove the property's documentation from "
            f"'{README_FILE_PATH}' and corresponding criteria from "
            f"'{CRITERIA_FILE_PATH}'"
        )

    def _add_property_to_properties_file(
        self, property_predecessor: str, property_name: str
    ) -> bool:
        """Adds a property to the properties file.

        Parameters
        ----------
        property_predecessor : str
            name of the property being the new property's predecessor or an empty
            string to position the new property at the first index
        property_name : str
            name of the property to be added

        Returns
        -------
        bool
            whether the addition succeeded
        """

        json_data = load_json_file(PROPERTIES_FILE_PATH)

        if json_data is None:
            return False

        properties = cast(list[str], json_data)
        index = 0

        if property_predecessor:
            index = properties.index(property_predecessor) + 1

        properties.insert(index, property_name)

        try:
            with open(PROPERTIES_FILE_PATH, "w") as properties_file:
                properties_file.write(convert_dict_to_json_string(properties))

        except OSError as e:
            print(
                f"Property '{property_name}' could not be added to "
                f"'{PROPERTIES_FILE_PATH}' because that file could not be "
                f"processed: {e}"
            )
            return False

        return True

    def _remove_property_from_properties_file(self, property_name: str) -> bool:
        """Removes a property from the properties file.

        Parameters
        ----------
        property_name : str
            name of the property to be removed

        Returns
        -------
        bool
            whether the removal succeeded
        """

        json_data = load_json_file(PROPERTIES_FILE_PATH)

        if json_data is None:
            return False

        properties = cast(list[str], json_data)
        properties.remove(property_name)

        try:
            with open(PROPERTIES_FILE_PATH, "w") as properties_file:
                properties_file.write(convert_dict_to_json_string(properties))

        except OSError as e:
            print(
                f"Property '{property_name}' could not be removed from "
                f"'{PROPERTIES_FILE_PATH}' because that file could not be "
                f"processed: {e}"
            )
            return False

        return True

    def _show_continue_prompt(self) -> None:
        """Shows a prompt until any key is pressed."""

        input("\nPress any key to continue...")

    def _clear(self) -> None:
        """Clears the terminal's content."""

        call("clear" if os.name == "posix" else "cls", shell=False)  # noqa: S603

    def _get_action(self) -> str:
        """Prompts for a user action (main menu)."""

        self._show_title_bar()
        print("What would you like to do?")
        print("[%s] Add property" % ACTION_ADD_PROPERTY)
        print("[%s] Remove property" % ACTION_REMOVE_PROPERTY)
        print("[%s] Quit" % ACTION_QUIT)

        return input("Action: ")

    def _get_property_positioning(self) -> bool:
        """Prompts for choosing the position of a new property

        Returns
        -------
        bool
            whether the property should be added as the first position
        """

        self._show_title_bar()
        print("Action: Add property\n")
        print("Where should the property be positioned?\n")
        print("[1] As the first property")
        print("[2] After a specific property")

        property_positioned_as_first = None
        while property_positioned_as_first not in ("1", "2"):
            property_positioned_as_first = input("\nInput: ")

        return bool(property_positioned_as_first == "1")

    def _get_property_predecessor(self) -> str:
        """Determines the position of a property by its predecessor

        Returns
        -------
        str
            the new property's predecessor
        """

        self._show_title_bar()
        print("Action: Add property\n")

        return input("Existing property to be the new property's predecessor: ")

    def _get_property_name(self) -> str:
        """Prompts for property name."""

        return input("Property name: ")

    def _get_input_type(self) -> InputType | None:
        """Prompts for property data type."""

        self._show_title_bar()
        print("Action: Add property\n")
        print("Please specify a data type for the new property\n")

        for input_type in InputType:
            print(f"[{input_type.value}] {input_type.name}")

        try:
            return InputType(int(input("\nData type: ")))
        except ValueError:
            return None

    def _get_property_default(self, input_type: InputType) -> Any:
        """Prompts for property default value (used for integers and booleans only)."""

        self._show_title_bar()
        print("Action: Add property\n")
        print(
            "Please specify a default value for the new property"
            "(examples: 'False', '-1')"
        )

        value = input("Default value: ")
        is_valid, value = self._validate_input_type(value, input_type)

        if is_valid:
            return value

        return None

    def _get_property_add_source(self) -> bool:
        """Prompts user whether to add a 'source' key or not"""

        self._show_title_bar()
        print("Action: Add property\n")
        print("Should a 'source' key be added to the property?\n")
        print("[1] Add 'source'")
        print("[2] Don't add 'source'")

        return bool(input("\nInput: ") == "1")

    def _show_title_bar(self) -> None:
        """Shows the programm's title bar."""

        self._clear()
        print(50 * "=")
        print("Manager for provider properties")
        print(50 * "=")

    def _validate_input_type(
        self, value: Any, input_type: InputType
    ) -> tuple[bool, None | int | bool]:
        """Validates input of value with input_type.

        Parameters
        ----------
        value : Any
            value to validate
        input_type : InputType
            data type to validate against

        Returns
        -------
        tuple[bool, None | int | bool]
            a tuple returning validation success and a Python data type
        """

        if input_type == InputType.INTEGER:
            try:
                return True, int(value)
            except ValueError:
                return False, None

        if input_type == InputType.BOOLEAN:
            if value.lower() == STRING_TRUE:
                return True, True
            if value.lower() == STRING_FALSE:
                return True, False

        return False, None
