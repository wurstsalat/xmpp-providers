#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Daniel Brötzmann <daniel.broetzmann@posteo.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Contains an HTTP/REST bot for provider ratings."""

from __future__ import annotations

from typing import cast

import logging
import random
import time
from datetime import datetime
from http import HTTPStatus

import requests
from bs4 import BeautifulSoup
from dns.rdtypes.IN.SRV import SRV
from dns.resolver import resolve

from tools.common import convert_dict_to_json_string
from tools.common import load_json_file
from tools.common import PROVIDERS_FILE_PATH
from tools.data_types import ProviderDetailsT

log = logging.getLogger()

XMPP_COMPLIANCE_TESTER_URL = "https://compliance.conversations.im/server/%s/"
XMPP_COMPLIANCE_TESTER_UPDATE_URL = "https://compliance.conversations.im/live/%s/"
GREEN_WEB_CHECK_URL = "https://admin.thegreenwebfoundation.org/greencheck/%s"

REQUEST_TIMEOUT_SECONDS = 10
MIN_UPDATE_TIMEOUT_SECONDS = 5
MAX_UPDATE_TIMEOUT_SECONDS = 10


class WebBot:
    """Queries or updates ratings for providers in the providers file and writes
    changes to the providers file.
    """

    def __init__(self, update: bool = False) -> None:
        """For some services, an update trigger is available (using --update):
          - XMPP Compliance Tester
        Query/Update can only be run separately.

        Parameters
        ----------
        update : bool
            whether to trigger a provider update
        """

        json_data = load_json_file(PROVIDERS_FILE_PATH)

        if json_data is None:
            return

        self._providers_data = cast(dict[str, ProviderDetailsT], json_data)
        self._updated_providers: set[str] = set()

        if update:
            self._trigger_updates()
            return

        self._check_providers()

    def get_updated_providers(self) -> set[str]:
        """Returns the updated providers.

        Returns
        -------
        set[str]
            updated providers
        """

        return self._updated_providers

    def _check_providers(self) -> None:
        """Checks the provider ratings for changes and writes them to
        the providers file.
        """

        log.info("Starting provider rating check")
        today = datetime.now().strftime("%Y-%m-%d")

        for provider, data in self._providers_data.items():
            current_xmpp_compliance_tester_rating = data["ratingXmppComplianceTester"][
                "content"
            ]

            # current_observatory_c2s = data[
            #    'ratingImObservatoryClientToServer']['content']
            # current_observatory_s2s = data[
            #    'ratingImObservatoryServerToServer']['content']

            current_green_web_check_rating = data["ratingGreenWebCheck"]["content"]

            new_xmpp_compliance_tester_rating = self._get_xmpp_compliance_tester_rating(
                provider
            )
            new_green_web_check_rating = self._get_green_web_check(provider)

            if (
                new_xmpp_compliance_tester_rating is not None
                and new_xmpp_compliance_tester_rating
                != current_xmpp_compliance_tester_rating
            ):
                log.info(
                    "%s: XMPP Compliance Tester rating changed from %s to %s",
                    provider,
                    current_xmpp_compliance_tester_rating,
                    new_xmpp_compliance_tester_rating,
                )
                data["ratingXmppComplianceTester"][
                    "content"
                ] = new_xmpp_compliance_tester_rating
                data["lastCheck"]["content"] = today
                self._updated_providers.add(provider)

            if (
                new_green_web_check_rating is not None
                and new_green_web_check_rating != current_green_web_check_rating
            ):
                log.info(
                    "%s: Green Web Check rating changed from %s to %s",
                    provider,
                    current_green_web_check_rating,
                    new_green_web_check_rating,
                )
                data["ratingGreenWebCheck"]["content"] = new_green_web_check_rating
                data["lastCheck"]["content"] = today
                self._updated_providers.add(provider)

        with open(PROVIDERS_FILE_PATH, "w", encoding="utf-8") as providers_file:
            formatted_json = convert_dict_to_json_string(self._providers_data)
            providers_file.write(formatted_json)

    @staticmethod
    def _get_xmpp_compliance_tester_rating(provider: str) -> int | None:
        """Queries XMPP Compliance Tester rating.

        Parameters
        ----------
        provider : str
            provider whose rating is to be retrieved

        Returns
        -------
        int | None
            provider rating or None on error
        """

        log.info("Querying XMPP Compliance Tester rating for %s", provider)

        url = XMPP_COMPLIANCE_TESTER_URL % provider
        log.debug("Sending request to %s", url)

        with requests.get(url, timeout=REQUEST_TIMEOUT_SECONDS) as response:
            status_code = response.status_code

            if status_code != HTTPStatus.OK:
                log.warning(
                    "Querying XMPP Compliance Tester rating for %s failed with status "
                    "code %s",
                    provider,
                    status_code,
                )
                return None

            soup = BeautifulSoup(response.text, "html.parser")
            result = soup.find("div", class_="stat_result")

            if result is None:
                return None

            return int(result.text.strip()[:-1])

    def _get_green_web_check(self, provider: str) -> bool | None:
        """Queries Green Web Check rating.

        Parameters
        ----------
        provider : str
            provider whose rating is to be retrieved

        Returns
        -------
        bool | None
            whether provider is rated as being green or None on error
        """

        log.info("Querying Green Web Check rating for %s", provider)

        xmpp_srv_url = self._get_xmpp_srv_url(provider)

        url = GREEN_WEB_CHECK_URL % xmpp_srv_url
        log.debug("Sending request to %s", url)

        with requests.get(url, timeout=REQUEST_TIMEOUT_SECONDS) as response:
            status_code = response.status_code

            if status_code != HTTPStatus.OK:
                log.warning(
                    "Querying Green Web Check rating for %s (%s) failed with "
                    "status code %s",
                    xmpp_srv_url,
                    provider,
                    status_code,
                )
                return None

            return response.json().get("green")

    @staticmethod
    def _get_xmpp_srv_url(provider: str) -> str:
        """Tries to resolve "_xmpp-client._tcp"
        (see https://datatracker.ietf.org/doc/html/rfc6120#section-3.2.1) or
        "_xmpps-client._tcp" (see https://xmpp.org/extensions/xep-0368.html)
        hostname and returns it.

        Parameters
        ----------
        provider : str
            provider whose SRV records are to be retrieved

        Returns
        -------
        str
            "_xmpp-client" or "_xmpps-client" hostname if available,
            otherwise the unmodified provider
        """

        answer = resolve(f"_xmpp-client._tcp.{provider}", "SRV")
        if answer.rrset is None:
            answer = resolve(f"_xmpps-client._tcp.{provider}", "SRV")

        if answer.rrset is not None:
            for record in cast(list[SRV], answer):
                url = record.target.to_text(omit_final_dot=True)
                log.info("Retrieved XMPP SRV URL from %s: %s", answer.name, url)
                return url
        return provider

    def _trigger_updates(self) -> None:
        """Triggers updates for provider ratings at services supporting updates.

        Currently supported:
        - XMPP Compliance Tester
        """

        log.info("Triggering provider rating updates")

        provider_count = len(self._providers_data)
        i = 1

        for provider in self._providers_data:
            self._update_xmpp_compliance_tester_rating(provider)

            if i < provider_count:
                i += 1
                timeout_seconds = random.randint(  # noqa: S311
                    MIN_UPDATE_TIMEOUT_SECONDS, MAX_UPDATE_TIMEOUT_SECONDS
                )

                log.debug(
                    "Waiting for %s seconds to trigger the next rating update",
                    timeout_seconds,
                )

                # Add timeout to avoid overloading the rating service.
                # The timeout is randomly chosen to avoid multiple bots querying the
                # same service with similar intervals.
                time.sleep(timeout_seconds)

    @staticmethod
    def _update_xmpp_compliance_tester_rating(provider: str) -> None:
        """Triggers an XMPP Compliance Tester check for updating a provider rating.

        Parameters
        ----------
        provider : str
            provider domain
        """

        log.info("Updating provider rating for %s", provider)

        url = XMPP_COMPLIANCE_TESTER_UPDATE_URL % provider
        log.debug("Sending request to %s", url)

        with requests.get(url, timeout=REQUEST_TIMEOUT_SECONDS) as response:
            status_code = response.status_code

            if status_code != HTTPStatus.OK:
                log.warning(
                    "Updating provider rating for %s failed with status code: %s",
                    provider,
                    status_code,
                )
