#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Daniel Brötzmann <daniel.broetzmann@posteo.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains types used for type annotations."""
from __future__ import annotations

from typing import TypedDict


class ProviderDetailsT(TypedDict):
    """Type for providers file: dict[str, ProviderDetailsT]"""

    lastCheck: LastCheckDetailsT
    website: DictStrDetailsT
    busFactor: IntDetailsT
    company: BoolDetailsT
    passwordReset: DictStrDetailsT
    ratingGreenWebCheck: BoolDetailsT
    inBandRegistration: BoolDetailsT
    registrationWebPage: DictStrDetailsT
    ratingXmppComplianceTester: IntDetailsT
    ratingImObservatoryClientToServer: StringDetailsT
    ratingImObservatoryServerToServer: StringDetailsT
    maximumHttpFileUploadFileSize: IntDetailsT
    maximumHttpFileUploadTotalSize: IntDetailsT
    maximumHttpFileUploadStorageTime: IntDetailsT
    maximumMessageArchiveManagementStorageTime: IntDetailsT
    professionalHosting: BoolDetailsT
    freeOfCharge: BoolDetailsT
    legalNotice: DictStrDetailsT
    serverLocations: ListStrDetailsT
    groupChatSupport: dict[str, SupportDetailsT]
    chatSupport: dict[str, SupportDetailsT]
    emailSupport: dict[str, SupportDetailsT]
    since: StringDetailsT


class StringDetailsT(TypedDict):
    """ProviderDetailsT for str content"""

    content: str
    source: str
    comment: str


class IntDetailsT(TypedDict):
    """ProviderDetailsT for int content"""

    content: int
    source: str
    comment: str


class BoolDetailsT(TypedDict):
    """ProviderDetailsT for bool content"""

    content: bool
    source: str
    comment: str


class ListStrDetailsT(TypedDict):
    """ProviderDetailsT for list[str] content"""

    content: list[str]
    source: str
    comment: str


class SupportDetailsT(TypedDict):
    """ProviderDetailsT for list[str] content used for support keys
    as dict[str, SupportDetailsT]
    """

    content: list[str]
    source: str
    comment: str


class DictStrDetailsT(TypedDict):
    """ProviderDetailsT for dict[str, str] content"""

    content: dict[str, str]
    comment: str


class LastCheckDetailsT(TypedDict):
    """ProviderDetailsT for lastCheck key"""

    content: str
    comment: str


class FilteredDetailsT(TypedDict):
    """Type for filtered providers (as found in filtered provider lists)"""

    jid: str
    category: str
    lastCheck: str
    website: dict[str, str]
    busFactor: int
    company: bool
    passwordReset: dict[str, str]
    ratingGreenWebCheck: bool
    inBandRegistration: bool
    registrationWebPage: dict[str, str]
    ratingXmppComplianceTester: int
    ratingImObservatoryClientToServer: str
    ratingImObservatoryServerToServer: str
    maximumHttpFileUploadFileSize: int
    maximumHttpFileUploadTotalSize: int
    maximumHttpFileUploadStorageTime: int
    maximumMessageArchiveManagementStorageTime: int
    professionalHosting: bool
    freeOfCharge: bool
    legalNotice: dict[str, str]
    serverLocations: list[str]
    groupChatSupport: dict[str, list[str]]
    chatSupport: dict[str, list[str]]
    emailSupport: dict[str, list[str]]
    since: str
